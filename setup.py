from setuptools import setup

setup(
    name='roleplay',
    packages=['roleplay'],
    include_package_data=True,
    install_requires=[
        'roleplay',
    ],
)
