from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api, reqparse
from flask_jwt_simple import JWTManager
from flask_migrate import Migrate

from roleplay import secure


app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'kj34hf2ddf2yu09d88fd11f'
app.config['SECRET_KEY'] = secure.SECRET_KEY
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = secure.SQLALCHEMY_DATABASE_URI

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)
parser = reqparse.RequestParser()
jwt = JWTManager(app)
