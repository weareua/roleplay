from flask_restful import Resource
from flask_jwt_simple import create_jwt
from roleplay.services.auth import LoginService
from roleplay.app import parser


class LoginAPI(Resource):
    """
    Login API
    """
    parser.add_argument('username')
    parser.add_argument('password')

    def post(self):
        args = parser.parse_args()
        username = args['username']
        password = args['password']
        if username is None or password is None:
            return {"msg": "Username or password is missing"}, 400  # Missing arguments
        # user = LoginService.get_user("isle@gmail.com", "warning")
        user = LoginService.get_user(username, password)
        if not user:
            return {"msg": "Wrong username or password"}, 400
        jwt = create_jwt(identity=username)
        return {"username": user.username, "First Name": user.first_name,
                "Last Name": user.last_name, "jwt": jwt}
