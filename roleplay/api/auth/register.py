from flask_restful import Resource
from flask_jwt_simple import create_jwt
from roleplay.app import parser
from roleplay.services.auth import RegisterService


class RegisterAPI(Resource):
    """
    Register API
    """
    parser.add_argument('username')
    parser.add_argument('first_name')
    parser.add_argument('last_name')
    parser.add_argument('password')

    def post(self):
        args = parser.parse_args()
        user = RegisterService.create_user(args['username'], args['first_name'],
            args['last_name'], args['password'])
        jwt = create_jwt(identity=user.username)
        return {"username": user.username, "First Name": user.first_name,
                "Last Name": user.last_name, "jwt": jwt}
