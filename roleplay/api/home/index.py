from flask_restful import Resource
from flask_jwt_simple import jwt_required, get_jwt_identity


class HomeAPI(Resource):
    """
    Home API
    """
    @jwt_required
    def get(self):
        return {'Greetings': get_jwt_identity()}