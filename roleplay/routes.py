from roleplay.app import api
from roleplay.api import auth
from roleplay.api import home


api.add_resource(auth.login.LoginAPI, '/login')
api.add_resource(auth.register.RegisterAPI, '/register')
api.add_resource(home.index.HomeAPI, '/')
