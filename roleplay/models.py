from werkzeug.security import generate_password_hash, check_password_hash
from roleplay.app import db


class User(db.Model):
    """
    Create User table
    """
    
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User: {}>'.format(self.username)


class Society(db.Model):
    """
    Create a Society table
    """

    __tablename__ = 'societies'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))

    def __repr__(self):
        return '<Society: {}>'.format(self.name)


class Role(db.Model):
    """
    Create a Role table
    """

    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    is_owner = db.Column(db.Boolean, default=False)


class Permissions(db.Model):
    """
    Create permissions for the role table
    """

    __tablename__ = 'permissions'

    id = db.Column(db.Integer, primary_key=True)
    post_related = db.Column(db.Boolean, default=False)
    delete_messages = db.Column(db.Boolean, default=False)
    remove_members = db.Column(db.Boolean, default=False)
    add_members = db.Column(db.Boolean, default=False)
    events_related = db.Column(db.Boolean, default=False)
    roles_related = db.Column(db.Boolean, default=False)

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    role = db.relationship('Role', backref=db.backref(
        'permissions', lazy="dynamic"))


class Member(db.Model):
    """
    Create a Member table
    """

    __tablename__ = 'members'

    id =db.Column(db.Integer, primary_key=True)
    role = db.Column(db.Integer, db.ForeignKey('roles.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref=db.backref(
        'members', lazy="dynamic"))
    society_id = db.Column(db.Integer, db.ForeignKey('societies.id'))
    society = db.relationship('Society', backref=db.backref(
        'members', lazy="dynamic"))
