from roleplay.app import db
from roleplay.models import User


class RegisterService:
    """
    Register user into db
    """

    @staticmethod
    def create_user(email, first_name, last_name, password):
        # set username as text before @ symbol
        terminator = email.index('@')
        username =  email[:terminator]

        user = User(
            username=username, email=email, first_name=first_name,
            last_name=last_name,  password=password)

        # add user to the database
        db.session.add(user)
        db.session.commit()

        return user


class LoginService:
    """
    Get user from db using credentials
    """

    @staticmethod
    def get_user(email, password):
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            return user
        else:
            return False
